import React from "react";
import StripeCheckout from "react-stripe-checkout";

const StripeCheckoutButton = ({ price }) => {
  const priceForStripe = price * 100;
  const publishableKey = "pk_test_N7lXsCzXLY62GvO6KNwyiJCt00PPAlmr7v";

  const onToken = token => {
    console.log(token);
    alert("paymante dome");
  };

  return (
    <StripeCheckout
      label="Pay Now"
      name="Shopping"
      image="https://sendeyo.com/up/d/f3eb2117da"
      billingAddress
      shippingAddress
      description={`Your total is $${price}`}
      amount={priceForStripe}
      panelLabel="Pay Now"
      token={onToken}
      stripeKey={publishableKey}
    />
  );
};

export default StripeCheckoutButton;
